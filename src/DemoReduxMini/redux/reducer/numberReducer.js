let initialState = {
  number: 1,
};

export const numberReducer = (state = initialState, action) => {
  switch (action.type) {
    case "TANG": {
      state.number++;
      return { ...state };
    }
    case "GIAM": {
      return { ...state, number: (state.number = state.number - 1) };
    }
    default: {
      return state;
    }
  }
};
