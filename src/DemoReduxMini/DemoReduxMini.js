import React, { Component } from "react";
import { connect } from "react-redux";

class DemoReduxMini extends Component {
  render() {
    return (
      <div>
        <button onClick={this.props.handleTang} className="btn btn-success">
          +
        </button>
        <strong className="mx-3">{this.props.currentNuber}</strong>
        <button
          onClick={() => {
            this.props.handleGiam(10);
          }}
          className="btn btn-danger"
        >
          -
        </button>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    currentNuber: state.soLuong.number,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleTang: () => {
      let action = {
        type: "TANG",
      };
      dispatch(action);
    },
    handleGiam: (number) => {
      let action = {
        type: "GIAM",
        payload: number,
      };
      dispatch(action);
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(DemoReduxMini);
